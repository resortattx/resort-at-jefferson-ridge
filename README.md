A commitment to style, charm, and you! The special touches at The Resort at Jefferson Ridge bring the Southwest alive with the warmth of massive adobe walls, ornate pillars and beautiful red tiled roofs. Inside each home, you're provided with amenities found only in the best apartment homes.

Address: 5301 North Macarthur Blvd, Irving, TX 75038, USA

Phone: 972-457-3003
